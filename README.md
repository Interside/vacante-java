
```

### 3.- Ejercicio
 MYSQL , PYTHON el siguientes casos de uso, siguiendo:

* Actualmente los clientes cuentan con una tarjeta electrónica, donde realizan recargas y comprar en cualquier punto de venta.

* Se solicita crear los siguientes casos de uso.
* (Endpoint api) Permitir agregar saldos si el cliente esta activo
* (Endpoint api) Permitir comprar un producto si este cuenta con saldo.
* (Endpoint api) Permitir y consultar el histórico de comprar y recargas.


```

1.- Cliente
Nombre : Eduardo Padilla
Cuenta: 12234444

2.- Compras
Productos: Coca
Precio : 3500

3.- Recarga
Cuenta : 12234444
Cantidad : 5000

4.- Histórico
   Eduardo Padilla  
   Cta :12234444 

```
 Vista Histórico
 
|Descripción| Compras| Recargas|
| --- | --- |--- |
|Recarga	| 		 | 5000    |
|Coca		| 3500	 |         |
|Papas		| 1500	 |         |
|Recarga	|        |  1000   |
|TOTAL	    |   5000 |  6000   |
|Saldo	    |    |  	+1000   |


